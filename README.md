# My NodeJS Backend Service

Here is a simple ExpressJS Microservice

`npm start`

## Routes:

### XML2JS
`http://localhost:3000/xml2json`

### Zip Code Distance
This is some boilerplate code that I based this project off of, after seeing that the npm "request" project is now depricated I decided to modify the code to use the popular "got" package instad

Source: https://nodesource.com/blog/microservices-in-nodejs

`
test with:
http://localhost:3000/distance/84010/88901
`