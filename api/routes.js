'use strict';

const controller = require('./controller');

module.exports = function(app) {
   app.route('/about')
       .get(controller.about);
   // some boilerplate code that I based my project off of visit the following route:
   // http://localhost:3000/distance/84010/97229
   app.route('/distance/:zipcode1/:zipcode2')
       .get(controller.getDistance);
    app.route('/xml2json')
        .get(controller.convertXMLToJSON);
};