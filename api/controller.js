'use strict';

var properties = require('../package.json');
var distance = require('../service/distance');
var converter = require('../service/xml2json');

var controllers = {
   about: function(res) {
       var aboutInfo = {
           name: properties.name,
           version: properties.version
       }
       res.json(aboutInfo);
   },
   getDistance: function(req, res) {
           distance.find(req, res, function(err, dist) {
               if (err)
                   res.send(err);
               res.json(dist);
           });
       },
    convertXMLToJSON: function(res) {
        converter.xml2json(res, function(err, dist) {
            if (err)
                res.send(err);
            res.json(dist);
        });
    },
};

module.exports = controllers;