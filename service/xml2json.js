const got = require('got');
const convert = require('xml-js');

const xml_url = 'https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=XML'


var converter = {
   xml2json: async (res) => {
        try {
            const response = await got(xml_url);
            const body = response.body;
            const converted = convert.xml2json(body);
            res.send(JSON.parse(converted));
        } catch (error) {
            console.log(error);
        }
   }
}

module.exports = converter;