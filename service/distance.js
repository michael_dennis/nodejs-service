const apiKey = process.env.ZIPCODE_API_KEY || "n3Cofk2N519KGubyPVDBwi2FbPrxbbDKcbslWx9DMFaP2vw1t30zFNNeQS8jsIiY";
const zipCodeURL = 'https://www.zipcodeapi.com/rest/';

const got = require('got');

var distance = {
   find: async (req, res) => {
        try {
            const response = await got(zipCodeURL + apiKey 
                + '/distance.json/' + req.params.zipcode1 + '/' 
                + req.params.zipcode2 + '/mile')
            const body = response.body;
            res.send(JSON.parse(body));
        } catch (error) {
            console.log(error);
        }
   }
}

module.exports = distance;